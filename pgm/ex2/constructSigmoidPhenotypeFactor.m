function phenotypeFactor = constructSigmoidPhenotypeFactor(alleleWeights, geneCopyVarOneList, geneCopyVarTwoList, phenotypeVar)
% This function takes a cell array of alleles' weights and constructs a 
% factor expressing a sigmoid CPD.
%
% You can assume that there are only 2 genes involved in the CPD.
%
% In the factor, for each gene, each allele assignment maps to the allele
% whose weight is at the corresponding location.  For example, for gene 1,
% allele assignment 1 maps to the allele whose weight is at
% alleleWeights{1}(1) (same as w_1^1), allele assignment 2 maps to the
% allele whose weight is at alleleWeights{1}(2) (same as w_2^1),....  
% 
% You may assume that there are 2 possible phenotypes.
% For the phenotypes, assignment 1 maps to having the physical trait, and
% assignment 2 maps to not having the physical trait.
%
% THE VARIABLE TO THE LEFT OF THE CONDITIONING BAR MUST BE THE FIRST
% VARIABLE IN THE .var FIELD FOR GRADING PURPOSES
%
% Input:
%   alleleWeights: Cell array of weights, where each entry is an 1 x n 
%   of weights for the alleles for a gene (n is the number of alleles for
%   the gene)
%   geneCopyVarOneList: m x 1 vector (m is the number of genes) of variable 
%   numbers that are the variable numbers for each of the first parent's 
%   copy of each gene (numbers in this list go in the .var part of the
%   factor)
%   geneCopyVarTwoList: m x 1 vector (m is the number of genes) of variable 
%   numbers that are the variable numbers for each of the second parent's 
%   copy of each gene (numbers in this list go in the .var part of the
%   factor) -- Note that both copies of each gene are from the same person,
%   but each copy originally came from a different parent
%   phenotypeVar: Variable number corresponding to the variable for the 
%   phenotype (goes in the .var part of the factor)
%
% Output:
%   phenotypeFactor: Factor in which the values are the probabilities of 
%   having each phenotype for each allele combination (note that this is 
%   the FULL CPD with no evidence observed)

phenotypeFactor = struct('var', [], 'card', [], 'val', []);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% INSERT YOUR CODE HERE
% Note that computeSigmoid.m will be useful for this function.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%  
% alleleWeights, geneCopyVarOneList, geneCopyVarTwoList, phenotypeVar
% Fill in phenotypeFactor.var.  This should be a 1-D row vector.

% geneCopyVarOneList - gene copy 1 of parent 1 and gene copy 1 of parent 2
% geneCopyVarTwoList - gene copy 2 of parent 1 and gene copy 2 of parent 2
phenotypeFactor.var = [phenotypeVar, geneCopyVarOneList', geneCopyVarTwoList'];
% Fill in phenotypeFactor.card.  This should be a 1-D row vector.
phenotypeFactor.card = repmat(length(alleleWeights), 1, length(phenotypeFactor.var));

phenotypeFactor.val = zeros(1, prod(phenotypeFactor.card));
% Replace the zeros in phentoypeFactor.val with the correct values.
assignments = IndexToAssignment(1:length(phenotypeFactor.val), phenotypeFactor.card);
p_indx = find(phenotypeFactor.var == phenotypeVar);
alpha_assignments = assignments(assignments(:, p_indx) == 1, :);

for i = 1 : length(alpha_assignments)
    alleles = alpha_assignments(i,2 : 5);
    % geneCopyVarOneList - gene copy 1 of parent 1 and gene copy 1 of
    % parent 2 {1, 2 - parent1 copy1, parent2 copy1}
    % geneCopyVarTwoList - gene copy 2 of parent 1 and gene copy 2 of parent 2
    % {4, 5 - parent1 copy2, parent2 copy2}
    % alleleWeights{1} - gene copy 1 & 2 of parent 1
    % alleleWeights{2} - gene copy 1 & 2 of parent 2
    z = alleleWeights{1}(alleles(1)) + alleleWeights{2}(alleles(2)) + ...
        alleleWeights{1}(alleles(3)) + alleleWeights{2}(alleles(4));
    prob = computeSigmoid(z);
    phenotypeFactor = SetValueOfAssignment(phenotypeFactor, ...
        alpha_assignments(i, :), prob);
end


phenotypeFactor = SetValueOfAssignment(phenotypeFactor, ...
    assignments(assignments(:, p_indx) ~= 1, :), ...
(1 - GetValueOfAssignment(phenotypeFactor, alpha_assignments)));

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%