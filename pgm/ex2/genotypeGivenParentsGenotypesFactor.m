function genotypeFactor = genotypeGivenParentsGenotypesFactor(numAlleles, genotypeVarChild, genotypeVarParentOne, genotypeVarParentTwo)
% This function computes a factor representing the CPD for the genotype of
% a child given the parents' genotypes.

% THE VARIABLE TO THE LEFT OF THE CONDITIONING BAR MUST BE THE FIRST
% VARIABLE IN THE .var FIELD FOR GRADING PURPOSES

% When writing this function, make sure to consider all possible genotypes 
% from both parents and all possible genotypes for the child.

% Input:
%   numAlleles: int that is the number of alleles
%   genotypeVarChild: Variable number corresponding to the variable for the
%   child's genotype (goes in the .var part of the factor)
%   genotypeVarParentOne: Variable number corresponding to the variable for
%   the first parent's genotype (goes in the .var part of the factor)
%   genotypeVarParentTwo: Variable number corresponding to the variable for
%   the second parent's genotype (goes in the .var part of the factor)
%
% Output:
%   genotypeFactor: Factor in which val is probability of the child having 
%   each genotype (note that this is the FULL CPD with no evidence 
%   observed)

% The number of genotypes is (number of alleles choose 2) + number of 
% alleles -- need to add number of alleles at the end to account for homozygotes

genotypeFactor = struct('var', [], 'card', [], 'val', []);

% allele = A= 1 a = 2 , id = 1 means AA
% Each allele has an ID.  Each genotype also has an ID.  We need allele and
% genotype IDs so that we know what genotype and alleles correspond to each
% probability in the .val part of the factor.  For example, the first entry
% in .val corresponds to the probability of having the genotype with
% genotype ID 1 (AA), which consists of having two copies of the allele with
% allele ID 1 (A), given that both parents also have the genotype with genotype
% ID 1 (AA,AA).  There is a mapping from a pair of allele IDs to genotype IDs and 
% from genotype IDs to a pair of allele IDs below; we compute this mapping 
% using generateAlleleGenotypeMappers(numAlleles). (A genotype consists of 
% 2 alleles.)

[allelesToGenotypes, genotypesToAlleles] = generateAlleleGenotypeMappers(numAlleles);

% allelesToGenotypes =
% 
%      1     2
%      2     3

% genotypesToAlleles =
% 
%      1     1
%      1     2
%      2     2


% One or both of these matrices might be useful.
%
%   1.  allelesToGenotypes: n x n matrix that maps pairs of allele IDs to 
%   genotype IDs, where n is the number of alleles -- if 
%   allelesToGenotypes(i, j) = k, then the genotype with ID k comprises of 
%   the alleles with IDs i and j
%
%   2.  genotypesToAlleles: m x 2 matrix of allele IDs, where m is the 
%   number of genotypes -- if genotypesToAlleles(k, :) = [i, j], then the 
%   genotype with ID k is comprised of the allele with ID i and the allele 
%   with ID j

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%INSERT YOUR CODE HERE
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%  

% Fill in genotypeFactor.var.  This should be a 1-D row vector.
genotypeFactor.var = [genotypeVarChild, genotypeVarParentOne, genotypeVarParentTwo];
% Fill in genotypeFactor.card.  This should be a 1-D row vector.
% function y = factorial(n)
%     if n == 0 || n == 1
%         y = 1;
%     else
%         while n > 1
%             y = n * (n -1);
%             n = n - 1;
%         end;
%     end;
% 
% Calculate n!/((n-2)!*2) - to get cardinality
n = numAlleles;
numer = 1;
while n >= 2
 numer = n * (n -1);
 n = n - 2;
end;

n = numAlleles - 2;
denom = 1;
while n >= 2
    denom = n * (n -1);
    n = n - 2;
end;    


genotypeNumber = floor(numer/(denom * 2)) + numAlleles;
genotypeFactor.card = [genotypeNumber, genotypeNumber, genotypeNumber];

genotypeFactor.val = zeros(1, prod(genotypeFactor.card));
% Replace the zeros in genotypeFactor.val with the correct values.
% probAssignFactor = [0.25 0.5 0.25];
% Gene = [AA, Aa, aa]
% alleles = [0.1, 0.9] A a
% genotypeFactor.val = [1,0,0,0.5,0.5,0,0,1,0,0.5,0.5,0,0.25,0.5,0.25,0,0.5,0.5,0,1,0,0,0.5,0.5,0,0,1];

%   P1      A1A1    A1A1    A1A1    A1a1    A1a1    A1a1    a1a1    a1a1    a1a1	
%   P2      A2A2    A2a2    a2a2    A2A2    A2a2    a2a2    A2A2    A2a2    a2a2
% 	AA       1      0.5     0       0.5     0.25    0       0       0       0 
%   Aa       0      0.5     1       0.5     0.5     0.5     1       0.5     0
%   aa       0      0       0       0       0.25    0.5     0       0.5     1
 
assignments = IndexToAssignment(1:length(genotypeFactor.val), genotypeFactor.card);

for i = 1: length(assignments)
    % get assignmnets for each row
    v = assignments(i,:);
    
    % Retrieve the gene Id mapped to allele id
    child_allele = genotypesToAlleles(v(1), :);
    parent1_allele = genotypesToAlleles(v(2), :);
    parent2_allele = genotypesToAlleles(v(3), :);
    
    % Calculate probabilities
    [x, y] = ndgrid(parent1_allele, parent2_allele);
    all_values = [x(:), y(:)]; 
    numer = 0;
    denom = length(all_values);
    for j = 1: denom
        if ~isempty(intersect(sort(child_allele(1,:)), sort(all_values(j,:)), 'rows'))
            numer = numer + 1;
        end;
    end;
    genotypeFactor.val(i) = (numer/denom);
end;



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%