// stdafx.h : include file for standard system include files,
// or project specific include files that are used frequently, but
// are changed infrequently
//

#pragma once

#include "targetver.h"

#define WIN32_LEAN_AND_MEAN             // Exclude rarely-used stuff from Windows headers
// Windows Header Files:
#include <windows.h>

// Exported R functions
extern "C" __declspec(dllexport) void __cdecl retrieveFileSize(const char** filePath, unsigned short* channel, char** msg, unsigned int *out);
extern "C" __declspec(dllexport) void __cdecl retrieveFile(const char** filePath, unsigned short* channel, char** msg, double *out);
extern "C" __declspec(dllexport) void __cdecl convertFile(const char** filePath, unsigned short* channel, char** msg, const char** savePath);
