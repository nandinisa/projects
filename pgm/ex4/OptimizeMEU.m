% Copyright (C) Daphne Koller, Stanford University, 2012

function [MEU OptimalDecisionRule] = OptimizeMEU( I )

  % Inputs: An influence diagram I with a single decision node and a single utility node.
  %         I.RandomFactors = list of factors for each random variable.  These are CPDs, with
  %              the child variable = D.var(1)
  %         I.DecisionFactors = factor for the decision node.
  %         I.UtilityFactors = list of factors representing conditional utilities.
  % Return value: the maximum expected utility of I and an optimal decision rule 
  % (represented again as a factor) that yields that expected utility.
  
  % We assume I has a single decision node.
  % You may assume that there is a unique optimal decision.
  D = I.DecisionFactors(1);
  
  OptimalDecisionRule =  struct('var', [], 'card', [], 'val', []); 
  OptimalDecisionRule.var = sort(D.var);
  OptimalDecisionRule.card = D.card;
  OptimalDecisionRule.val = zeros(1, prod(OptimalDecisionRule.card));
  
  MEU = 0;  
  %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  %
  % YOUR CODE HERE...
  % 
  % Some other information that might be useful for some implementations
  % (note that there are multiple ways to implement this):
  % 1.  It is probably easiest to think of two cases - D has parents and D 
  %     has no parents.
  % 2.  You may find the Matlab/Octave function setdiff useful.
  %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%    
    EU = CalculateExpectedUtilityFactor(I);    
    
    if(length(D(1).var) > 1)          
        assignments = IndexToAssignment(1: prod(EU.card), EU.card);
        d_var = D.var(1);   
        % Get all variables that are not decision nodes
        var = setdiff(EU.var, d_var);
        
        for i=1:OptimalDecisionRule.card(1)
            % Get index of variables that are not decision nodes
            [tf, idx] = ismember(var, EU.var);
            % Get all assignments where the value of the variable = i, i.e
            % [1, 2.. etc], then get corresponding indexes
            selected_assign = assignments(assignments(:, idx)==i, :);
            selected_indx = AssignmentToIndex(selected_assign, EU.card);
            % Get the max index location of that variable, and set the
            % corresponding decision node value = 1
            % i.e. 1 1 = 7.5 and 1 2 = 2.1, then max = 7.5. so when X = X1,
            % D = d1 and not d2.
            max_indx = find(EU.val == max(EU.val(selected_indx)));            
            OptimalDecisionRule.val(max_indx) = 1;            
        end
        
        MEU=sum(EU.val .* OptimalDecisionRule.val);

    else
        MEU = max(EU.val);
        idx = find(EU.val == MEU);
        OptimalDecisionRule.val(idx) = 1;
    end
       
end
