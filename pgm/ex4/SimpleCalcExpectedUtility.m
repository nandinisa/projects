% Copyright (C) Daphne Koller, Stanford University, 2012

function EU = SimpleCalcExpectedUtility(I)

  % Inputs: An influence diagram, I (as described in the writeup).
  %         I.RandomFactors = list of factors for each random variable.  These are CPDs, with
  %              the child variable = D.var(1)
  %         I.DecisionFactors = factor for the decision node.
  %         I.UtilityFactors = list of factors representing conditional utilities.
  % Return Value: the expected utility of I
  % Given a fully instantiated influence diagram with a single utility node and decision node,
  % calculate and return the expected utility.  Note - assumes that the decision rule for the 
  % decision node is fully assigned.

  % In this function, we assume there is only one utility node.
  F = [I.RandomFactors I.DecisionFactors];
  U = I.UtilityFactors(1);
  EU = [];
  %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  %
  % YOUR CODE HERE
  %
  %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  %  Decision node cardinality becomes column
  %  X node cardinality becomes row  

  JP = F(1);

  % Get Joint distribution of all variables
  for i = 2: length(F)
      CPD = F(i);
%       if(issorted(CPD.var) == 0)
%           CPD = CPDFromFactor(CPD, CPD.var(1));
%       end
      JP = FactorProduct(JP, CPD);
  end
  
  marginal = setdiff([F(:).var], U.var);
  
  % Marginalize all variables not used in utility 
  for i = 1: length(marginal)
      JP = VariableElimination(JP, marginal(i));
  end

  utility = U.val;
 if(issorted(U.var) == 0)
    % Need to change index assignments i.e. U.var = [10, 9]
    % while JP.var = [9, 10]
    assignments = IndexToAssignment(1: prod(U.card), U.card);
    [tf, idx] = ismember(U.var, sort(U.var));
    % Swap the indexes
    assignments = assignments(:, idx);
    
    for i = 1: length(assignments)
        idx = AssignmentToIndex(assignments(i, :), JP.card);
        utility(i) = U.val(idx);
    end
 end
 
EU = sum(JP.val .* utility);
end
