% Copyright (C) Daphne Koller, Stanford University, 2012

function EUF = CalculateExpectedUtilityFactor(I)

  % Inputs: An influence diagram I with a single decision node and a single utility node.
  %         I.RandomFactors = list of factors for each random variable.  These are CPDs, with
  %              the child variable = D.var(1)
  %         I.DecisionFactors = factor for the decision node.
  %         I.UtilityFactors = list of factors representing conditional utilities.
  % Return value: A factor over the scope of the decision rule D from I that
  % gives the conditional utility given each assignment for D.var
  %
  % Note - We assume I has a single decision node and utility node.
  EUF = [];
  %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  %
  % YOUR CODE HERE...
  %
  %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%  

%   EUF = struct('var', [], 'card', [], 'val', []);
%   EUF.var = I.DecisionFactors(1).var;
%   EUF.card = I.DecisionFactors(1).card;
%   EUF.val = zeros(1, I.DecisionFactors(1).card);
%   AllDs = I.DecisionFactors;  
%   for i=1:length(AllDs)
%       I.DecisionFactors = AllDs(i);
%       EUF.val(i) = SimpleCalcExpectedUtility(I);
%   end  


F = [I.RandomFactors I.UtilityFactors];
D = I.DecisionFactors(1);
JP = F(1);

  % Get Joint distribution of all variables
  for i = 2: length(F)
      CPD = F(i);
%       if(issorted(CPD.var) == 0)
%           CPD = CPDFromFactor(CPD, CPD.var(1));
%       end
      JP = FactorProduct(JP, CPD);
  end
  
  marginal = setdiff([F(:).var], D.var);
  
  % Marginalize all variables not used in utility 
  for i = 1: length(marginal)
      JP = VariableElimination(JP, marginal(i));
  end

EUF = JP; 
end  
