#include "stdafx.h"			// standard stuff
#include <stdio.h>
#include <conio.h>
#include <iostream>			// basic console I/O
#include <atlcomcli.h>
#include <fstream>
#include <vector>
#include <typeinfo>

using namespace std;
// #import "RecordingInterface.olb" no_namespace
#import "libid:8098371E-98AD-0070-BEF3-21B9A51D6B3E" no_namespace
// #import "percPNRFLoader.dll" no_namespace
#import "libid:8098371E-98AD-0062-BEF3-21B9A51D6B3E" no_namespace


/* Start of private variables and methods */


// Ref: https://erpcoder.wordpress.com/2016/06/15/how-to-develop-a-c-dll-for-r-in-visual-studio-2015/
IRecordingPtr loadRecording(const char* filePath) {
	IRecordingLoaderPtr itfLoader;
	itfLoader.CreateInstance(__uuidof(PNRFLoader));
	IRecordingPtr itfRecording = itfLoader->LoadRecording(filePath);
	return itfRecording;
}

CComVariant retrieveDataSource(IRecordingPtr &itfRecording, unsigned short channel) {
	double dStart, dEnd;
	// connect to data source channel 1
	IDataSrcPtr dataSource = itfRecording->Channels->Item[channel]->DataSource[DataSourceSelect_Mixed];
	// fetch start and stop time
	dataSource->Sweeps->get_StartTime(&dStart);
	dataSource->Sweeps->get_EndTime(&dEnd);

	CComVariant data;
	// Get data between start and stop time
	dataSource->Data(dStart, dEnd, &data);

	return data;
}

void saveData(CComVariant &data, const char* filePath){
	// if object is empty: quit
	if (data.vt != VT_EMPTY)
	{
		// create segments pointer
		IDataSegmentsPtr itfSegments = data.punkVal;
		int iSegIndex = 1;					// segment index
		int iCount = itfSegments->Count;	// segemnt count
		if (iCount <= 1)
		{
			throw("No Segments found");
			return;
		}

		ofstream pnrfFormattedfile;
		pnrfFormattedfile.open(filePath);
		pnrfFormattedfile << "X, Y\n";

		// loop through all available segments
		for (iSegIndex = 1; iSegIndex <= iCount; iSegIndex++)
		{
			// pointer inside segment data
			IDataSegmentPtr itfSegment = NULL;
			itfSegments->get_Item(iSegIndex, &itfSegment);

			int lCnt = itfSegment->NumberOfSamples;

			// variant data array for segment data
			CComVariant varData;
			// fetch data
			itfSegment->Waveform(DataSourceResultType_Double64, 1, lCnt, 1, &varData);

			//If there is no data, process next segment
			if (varData.vt == VT_EMPTY)
				continue;

			//If it isn't an array, something is wrong here
			if (!(varData.vt & VT_ARRAY))
				continue;

			//Get data out through the use of the safe array and store locally
			SAFEARRAY* satmp = NULL;
			satmp = varData.parray;

			if (satmp->cDims > 1)
			{
				// It's a multi dimensional array
				continue;
			}
			double *pData;
			SafeArrayAccessData(satmp, (void**)&pData);

			double X0 = itfSegment->StartTime;
			double DeltaX = itfSegment->SampleInterval;
			double X, Y;
			
			for (int i = 0; i < (int)satmp->rgsabound[0].cElements; i++)
			{
				X = X0 + i * DeltaX;
				Y = pData[i];
				pnrfFormattedfile << X << "," << Y << "\n";
			}
			SafeArrayUnaccessData(satmp);
		}

		pnrfFormattedfile.close();
	}
}

// set density vector with a constant value
vector<double> setData(vector<double> val, const double & pData){
	vector<double>::iterator it;
	for (it = val.begin(); it != val.end(); it++) *it = pData;
	return val;
}

unsigned int getDataSize(CComVariant &data) {
	unsigned int out = 0;
	if (data.vt != VT_EMPTY)
	{
		// create segments pointer
		IDataSegmentsPtr itfSegments = data.punkVal;
		int iSegIndex = 1;					// segment index
		int iCount = itfSegments->Count;	// segemnt count
		if (iCount <= 1)
		{
			throw("No Segments found");
		}

		// loop through all available segments
		for (iSegIndex = 1; iSegIndex <= iCount; iSegIndex++)
		{
			// pointer inside segment data
			IDataSegmentPtr itfSegment = NULL;
			itfSegments->get_Item(iSegIndex, &itfSegment);

			int lCnt = itfSegment->NumberOfSamples;

			// variant data array for segment data
			CComVariant varData;
			// fetch data
			itfSegment->Waveform(DataSourceResultType_Double64, 1, lCnt, 1, &varData);

			//If there is no data, process next segment
			if (varData.vt == VT_EMPTY)
				continue;

			//If it isn't an array, something is wrong here
			if (!(varData.vt & VT_ARRAY))
				continue;

			out = out + lCnt;
		}
	}

	return out;
}

void loadData(CComVariant &data, double *out){
	if (data.vt != VT_EMPTY)
	{
		// create segments pointer
		IDataSegmentsPtr itfSegments = data.punkVal;
		int iSegIndex = 1;					// segment index
		int iCount = itfSegments->Count;	// segemnt count
		if (iCount <= 1)
		{
			throw("No Segments found");
			return;
		}

		vector<double> val;
		
		// loop through all available segments
		for (iSegIndex = 1; iSegIndex <= iCount; iSegIndex++)
		{
			// pointer inside segment data
			IDataSegmentPtr itfSegment = NULL;
			itfSegments->get_Item(iSegIndex, &itfSegment);

			int lCnt = itfSegment->NumberOfSamples;

			// variant data array for segment data
			CComVariant varData;
			// fetch data
			itfSegment->Waveform(DataSourceResultType_Double64, 1, lCnt, 1, &varData);

			//If there is no data, process next segment
			if (varData.vt == VT_EMPTY)
				continue;

			//If it isn't an array, something is wrong here
			if (!(varData.vt & VT_ARRAY))
				continue;

			//Get data out through the use of the safe array and store locally
			SAFEARRAY* satmp = NULL;
			satmp = varData.parray;

			if (satmp->cDims > 1)
			{
				// It's a multi dimensional array
				continue;
			}

			double *pData;
			SafeArrayAccessData(satmp, (void**)&pData);
			val.insert(val.end(), pData, pData + lCnt - 1);
			SafeArrayUnaccessData(satmp);
		}

		//out = new double[val.size()];
		for (int i = 0; i < val.size(); i++) {
			out[i] = val[i];
		}
	}
}

void setError(_com_error err, char** msg) {
	wstring errMsg(err.ErrorMessage());
	*msg = new char[255];
	sprintf(*msg, "%ls", errMsg.c_str());
}

/* End of private variables and methods */

/* Start of public variables and methods */

// Read the PNRF file path and specified channel, and save it as a file
// filepath = "C:\\Temp\\VT609.pnrf"
void convertFile(const char** filePath, unsigned short* channel, char** msg, const char** savePath) {
	HRESULT hr = CoInitializeEx(NULL, COINIT_APARTMENTTHREADED);

	try {	
		IRecordingPtr itfRecording = loadRecording(filePath[0]);
		CComVariant data = retrieveDataSource(itfRecording, *channel);
		saveData(data, savePath[0]);
	}
	catch (char* e) {
		*msg = e;
	}
	catch (const _com_error& e) {		
		setError(e, msg);
	}

	if (hr == S_OK)
		CoUninitialize();
}


void retrieveFileSize(const char** filePath, unsigned short* channel, char** msg, unsigned int* out) {
	HRESULT hr = CoInitializeEx(NULL, COINIT_APARTMENTTHREADED);

	try {
		IRecordingPtr itfRecording = loadRecording(filePath[0]);
		CComVariant data = retrieveDataSource(itfRecording, *channel);
		*out = getDataSize(data);
	
		if (hr == S_OK)
			CoUninitialize();
	}
	catch (char* e) {
		*msg = e;
	}
	catch (const _com_error& e) {
		//wstring desc(e.Description());
		setError(e, msg);
		out[0] = -1;
	}

	if (hr == S_OK)
		CoUninitialize();
}

// Reads the PNRF file path and specified channel, and set the output variable the defined data in the channel
void retrieveFile(const char** filePath, unsigned short* channel, char** msg, double* out) {
	HRESULT hr = CoInitializeEx(NULL, COINIT_APARTMENTTHREADED);

	try {
		
		IRecordingPtr itfRecording = loadRecording(filePath[0]);
		CComVariant data = retrieveDataSource(itfRecording, *channel);
		loadData(data, out);
	}
	catch (char* e) {
		*msg = e;
	}
	catch (const _com_error& e) {	
		setError(e, msg);
		out[0] = -1;
	}
	
	if (hr == S_OK)
		CoUninitialize();

}

/* End of private variables and methods */